﻿using Cwiczenia6.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Cwiczenia6
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public List<Emp> Emps { get; set; }
        public List<Dept> Depts { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            LoadData();
            Example();
        }

        public void LoadData()
        {
            Emps = new List<Emp>();
            Depts = new List<Dept>();

            Emps.Add(new Emp
            {
                Empno = 7369,
                Ename = "SMITH",
                Job = "CLERK",
                Mgr = 7902,
                HireDate = new DateTime(1980, 12, 17),
                Sal=800,
                Comm=0,
                Deptno=20
            });

            Emps.Add(new Emp
            {
                Empno = 7499,
                Ename = "ALLEN",
                Job = "SALESMAN",
                Mgr = 7698,
                HireDate = new DateTime(1981, 2, 20),
                Sal = 1600,
                Comm = 300,
                Deptno = 30
            });

            Emps.Add(new Emp
            {
                Empno = 7521,
                Ename = "WARD",
                Job = "SALESMAN",
                Mgr = 7698,
                HireDate = new DateTime(1981, 2, 22),
                Sal = 1250,
                Comm = 500,
                Deptno = 30
            });

            Emps.Add(new Emp
            {
                Empno = 7566,
                Ename = "JONES",
                Job = "MANAGER",
                Mgr = 7839,
                HireDate = new DateTime(1981, 4, 2),
                Sal = 2975,
                Comm = 0,
                Deptno = 20
            });

            Emps.Add(new Emp
            {
                Empno = 7654,
                Ename = "MARTIN",
                Job = "SALESMAN",
                Mgr = 7698,
                HireDate = new DateTime(1981, 9, 28),
                Sal = 1250,
                Comm = 1400,
                Deptno = 30
            });

            Emps.Add(new Emp
            {
                Empno = 7698,
                Ename = "BLAKE",
                Job = "MANAGER",
                Mgr = 7839,
                HireDate = new DateTime(1981, 5, 1),
                Sal = 2850,
                Comm = 0,
                Deptno = 30
            });

            Emps.Add(new Emp
            {
                Empno = 7782,
                Ename = "CLARK",
                Job = "MANAGER",
                Mgr = 7839,
                HireDate = new DateTime(1981, 6, 9),
                Sal = 2450,
                Comm = 0,
                Deptno = 10
            });

            Emps.Add(new Emp
            {
                Empno = 7788,
                Ename = "SCOTT",
                Job = "ANALYST",
                Mgr = 7566,
                HireDate = new DateTime(1982, 12, 9),
                Sal = 3000,
                Comm = 0,
                Deptno = 20
            });

            Emps.Add(new Emp
            {
                Empno = 7839,
                Ename = "KING",
                Job = "PRESIDENT",
                Mgr = null,
                HireDate = new DateTime(1981, 11, 17),
                Sal = 5000,
                Comm = 0,
                Deptno = 10
            });

            Emps.Add(new Emp
            {
                Empno = 7844,
                Ename = "TURNER",
                Job = "SALESMAN",
                Mgr = 7698,
                HireDate = new DateTime(1981, 9, 8),
                Sal = 1500,
                Comm = 0,
                Deptno = 30
            });

            Emps.Add(new Emp
            {
                Empno = 7876,
                Ename = "ADAMS",
                Job = "CLERK",
                Mgr = 7788,
                HireDate = new DateTime(1983, 1, 12),
                Sal = 1100,
                Comm = 0,
                Deptno = 20
            });

            Emps.Add(new Emp
            {
                Empno = 7900,
                Ename = "JAMES",
                Job = "CLERK",
                Mgr = 7698,
                HireDate = new DateTime(1981, 12, 3),
                Sal = 950,
                Comm = 0,
                Deptno = 30
            });

            Emps.Add(new Emp
            {
                Empno = 7902,
                Ename = "FORD",
                Job = "ANALYST",
                Mgr = 7566,
                HireDate= new DateTime(1981, 12, 3),
                Sal = 3000,
                Comm = 0,
                Deptno = 20
            });

            Emps.Add(new Emp
            {
                Empno = 7934,
                Ename = "MILLER",
                Job = "CLERK",
                Mgr = 7782,
                HireDate = new DateTime(1982, 1, 23),
                Sal = 1300,
                Comm = 0,
                Deptno = 10
            });

            Depts.Add(new Dept
            {
                Deptno=10,
                Dname= "ACCOUNTING",
                Loc= "NEW YORK"
            });

            Depts.Add(new Dept
            {
                Deptno = 20,
                Dname = "RESEARCH",
                Loc = "DALLAS"
            });

            Depts.Add(new Dept
            {
                Deptno = 30,
                Dname = "SALES",
                Loc = "CHICAGO"
            });

            Depts.Add(new Dept
            {
                Deptno = 40,
                Dname = "OPERATIONS",
                Loc = "BOSTON"
            });
        }

        private void Example()
        {
            //Query syntax
            var result = from e in Emps
                         where e.Ename.StartsWith("K")
                         select e;

            //Lambda and Extension methods syntax
            var result2 = Emps.Where(e => e.Ename.StartsWith("K"));


            DataGrid.ItemsSource = result.ToList();
        }

        private void Query1(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         select new { R_PENSJA = (e.Sal * 12) };
                         

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query2(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         select new { EMPLOYEE = e.Empno.ToString() + " " + e.Ename.ToString() };

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query3(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         select new { Pracuje_w = e.Ename.ToString() + " pracuje w dziale " + e.Deptno.ToString() };

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query4(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         select new { Pensja = ((e.Sal * 12) + e.Comm) };

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query6(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         select new { e.Deptno };

            var result1 = result.ToArray().Distinct();

            DataGrid.ItemsSource = result1.ToList();
        }

        private void Query5(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         select new { e.Deptno };

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query7(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         select new { e.Deptno, e.Ename };

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query8(object sender, RoutedEventArgs ex)
        {
            var result = Emps.OrderBy(e => e.Ename);

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query9(object sender, RoutedEventArgs ex)
        {
            var result = Emps.OrderByDescending(e => e.HireDate);

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query10(object sender, RoutedEventArgs ex)
        {
            var result = Emps.OrderBy(e => e.Deptno).OrderByDescending(e => e.Sal);

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query11(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         where e.Job == "CLERK"
                         select new { e.Ename, e.Empno, e.Job, e.Deptno };

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query12(object sender, RoutedEventArgs ex)
        {
            var result = from e in Depts
                         where e.Deptno > 20
                         select new { e.Dname, e.Deptno };

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query13(object sender, RoutedEventArgs ex)
        {
            var result = Emps.Where(e => e.Comm > e.Sal);

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query14(object sender, RoutedEventArgs ex)
        {
            var result = Emps.Where(e => e.Sal > 1000 && e.Sal < 2000);

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query15(object sender, RoutedEventArgs ex)
        {
            var result = Emps.Where(e => e.Mgr == 7902 || e.Mgr == 7566 || e.Mgr == 7788);

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query16(object sender, RoutedEventArgs ex)
        {
            var result = Emps.Where(e => e.Ename.StartsWith("S"));

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query17(object sender, RoutedEventArgs ex)
        {
            var result = Emps.Where(e => e.Ename.Length == 4);

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query18(object sender, RoutedEventArgs ex)
        {
            var result = Emps.Where(e => e.Mgr == null);

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query19(object sender, RoutedEventArgs ex)
        {
            var result = Emps.Where(e => e.Sal < 1000 || e.Sal > 2000);

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query20(object sender, RoutedEventArgs ex)
        {
            var result = Emps.Where(e => !(e.Ename.StartsWith("M")));

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query21(object sender, RoutedEventArgs ex)
        {
            var result = Emps.Where(e => e.Mgr != null);

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query22(object sender, RoutedEventArgs ex)
        {
            var result = Emps.Where(e => e.Job == "CLERK" && (e.Sal > 1000 && e.Sal < 2000));

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query23(object sender, RoutedEventArgs ex)
        {
            var result = Emps.Where(e => e.Job == "CLERK" || (e.Sal > 1000 && e.Sal < 2000));

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query24(object sender, RoutedEventArgs ex)
        {
            var result = Emps.Where(e => (e.Job == "MANAGER" && e.Sal > 1500) || e.Job == "SALESMAN");

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query25(object sender, RoutedEventArgs ex)
        {
            var result = Emps.Where(e => (e.Job == "MANAGER" || e.Job == "SALESMAN") && e.Sal > 1500);

            DataGrid.ItemsSource = result.ToList();
        }

        private void Query26(object sender, RoutedEventArgs ex)
        {
            var result = Emps.Where(e => e.Job == "MANAGER" || (e.Job == "CLERK" && e.Deptno == 10) );

            DataGrid.ItemsSource = result.ToList();
        }

        private void Join1(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         join d in Depts on e.Deptno equals d.Deptno
                         select new {
                             e.Empno,
                             e.Ename,
                             e.Job,
                             e.Mgr,
                             e.Sal,
                             e.Comm,
                             e.HireDate,
                             e.Deptno,
                             d.Dname,
                             d.Loc
                         };

            DataGrid.ItemsSource = result.ToList();
        }

        private void Join2(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         join d in Depts on e.Deptno equals d.Deptno
                         orderby e.Ename
                         select new { e.Ename, d.Dname };
            
            DataGrid.ItemsSource = result.ToList();
        }

        private void Join3(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         join d in Depts on e.Deptno equals d.Deptno
                         select new { e.Ename, d.Dname, d.Deptno };
            DataGrid.ItemsSource = result.ToList();
        }

        private void Join4(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         join d in Depts on e.Deptno equals d.Deptno
                         where e.Sal > 1500
                         select new { e.Ename, d.Loc, d.Dname };
            DataGrid.ItemsSource = result.ToList();
        }

        private void Join5(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         join d in Depts on e.Deptno equals d.Deptno
                         where d.Loc == "DALLAS"
                         select new {
                             e.Empno,
                             e.Ename,
                             e.Job,
                             e.Mgr,
                             e.Sal,
                             e.Comm,
                             e.HireDate,
                             e.Deptno,
                             d.Loc };
            DataGrid.ItemsSource = result.ToList();
        }

        private void Join6(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         from d in Depts
                         select new {
                             e.Empno,
                             e.Ename,
                             e.Job,
                             e.Mgr,
                             e.Sal,
                             e.Comm,
                             e.HireDate,
                             e.Deptno,
                             d.Dname,
                             d.Loc
                         };
            DataGrid.ItemsSource = result.ToList();
        }

        private void Join7(object sender, RoutedEventArgs ex)
        { 
            var result = from d in Depts
                         from e in Emps
                         select new {
                             d.Deptno,
                             d.Dname,
                             d.Loc,
                             e.Empno,
                             e.Ename,
                             e.Job,
                             e.Mgr,
                             e.Sal,
                             e.Comm,
                             e.HireDate
                         };

            DataGrid.ItemsSource = result.ToList();
        }

        private void Join8(object sender, RoutedEventArgs ex)
        { 
            var result2 = (from e in Emps
                          where e.Deptno == 20
                          let p = " "
                          select new { p, e.Deptno }).Union(
                            from e in Emps
                            where e.Deptno == 30
                            let p = e.Ename
                            select new { p, e.Deptno });

            DataGrid.ItemsSource = result2.ToList();    
      
        }

        private void Join9(object sender, RoutedEventArgs ex)
        {
            var result = (from e in Emps
                          join d in Depts on e.Deptno equals d.Deptno
                          where (e.Deptno == 10 || e.Deptno == 30)
                          select new { e.Job });

            DataGrid.ItemsSource = result.ToList();
        }

        private void Join10(object sender, RoutedEventArgs ex)
        {
            var result = (from e in Emps
                          join d in Depts on e.Deptno equals d.Deptno
                          where (e.Deptno == 10 || e.Deptno == 30)
                          select new { e.Job }).Distinct();

            DataGrid.ItemsSource = result.ToList();
        }

        private void Join11(object sender, RoutedEventArgs ex)
        {

            var result = (from e in Emps
                          join d in Depts on e.Deptno equals d.Deptno
                          where (e.Deptno == 10 && e.Deptno != 30)
                          select new { e.Job }).Distinct();

            DataGrid.ItemsSource = result.ToList();
        }

        private void Join12(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         join d in Emps on e.Mgr equals d.Empno
                         where e.Sal < d.Sal
                         select e;
            DataGrid.ItemsSource = result.ToList();
        }

        private void Join13(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         join d in Emps on e.Mgr equals d.Empno
                         orderby d.Ename
                         select new { e.Ename, Mgr = d.Ename };

            DataGrid.ItemsSource = result.ToList();
        }

        private void Group1(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         select new { AvgSal = Emps.Average(s => s.Sal) };

            DataGrid.ItemsSource = result.Take(1).ToList();
            
        }

        private void Group2(object sender, RoutedEventArgs ex)
        {
            var result = from m in Emps
                         select new { SalMin = Emps.Where(e => e.Job == "CLERK").Select(e => e.Sal).Min() };

            DataGrid.ItemsSource = result.Take(1).ToList();
            
        }

        private void Group3(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         select new
                         {
                             Str = "Count of emps in dept 20",
                             Count = (from s in Emps
                                      where s.Deptno == 20
                                      select s.Empno).Count()
                         };

            DataGrid.ItemsSource = result.Take(1).ToList();
        }

        private void Group4(object sender, RoutedEventArgs ex)
        {
            var result = (from e in Emps
                          group e by e.Job into grp
                          select new { Job = grp.Key,
                              AvgSal = Emps.Where(e => e.Job == grp.Key).Average(e => e.Sal)});

            DataGrid.ItemsSource = result.ToList();
        }

        private void Group5(object sender, RoutedEventArgs ex)
        {
            var result = (from e in Emps
                          where e.Job != "MANAGER"
                          group e by e.Job into grp
                          select new
                          {
                              Job = grp.Key,
                              AvgSal = Emps.Where(e => e.Job == grp.Key).Average(e => e.Sal)
                          });

            DataGrid.ItemsSource = result.ToList();
        }

        private void Group6(object sender, RoutedEventArgs ex)
        {
            var result = (from e in Emps
                          group e by new { e.Job, e.Deptno } into grp
                          orderby grp.Key.Deptno
                          select new
                          {
                              Dept = grp.Key.Deptno,
                              Jobs = grp.Key.Job,
                              AvgSal = grp.Average(e => e.Sal)
                          });

            DataGrid.ItemsSource = result.ToList();
        }

        private void Group7(object sender, RoutedEventArgs ex)
        {
            var result = (from e in Emps
                          group e by e.Job into grp
                          select new
                          {
                              Job = grp.Key,
                              AvgSal = Emps.Where(e => e.Job == grp.Key).Max(e => e.Sal)
                          });

            DataGrid.ItemsSource = result.ToList();
        }
        private void Group8(object sender, RoutedEventArgs ex)
        {
            var result = (from e in Emps
                          group e by e.Deptno into grp
                          where grp.Count() > 3
                          select new
                          {
                              grp.Key,
                              AvgSal = Emps.Where(e => e.Deptno == grp.Key).Average(e => e.Sal)
                          });

            DataGrid.ItemsSource = result.ToList();
        }
        private void Group9(object sender, RoutedEventArgs ex)
        {
            var result = (from e in Emps
                          group e by e.Job into grp
                          where grp.Average(m => m.Sal) >= 3000
                          select new
                          {
                              grp.Key,
                              AvgSal = grp.Select(e => e.Sal).Average()
                          }).Distinct();

            DataGrid.ItemsSource = result.ToList();
        }
        private void Group10(object sender, RoutedEventArgs ex)
        {
            var result = (from e in Emps
             group e by e.Job into grp
             select new
             {
                 grp.Key,
                 AvgSalMonth = grp.Select(e => e.Sal + e.Comm).Average(),
                 AvgSalYear = grp.Select(e => (e.Sal * 12) + e.Comm).Average()
             });

            DataGrid.ItemsSource = result.ToList();
        }

        private void Group11(object sender, RoutedEventArgs ex)
        {
            var result = from x in Emps
                         select new { Str = "Roznica miedzy Max Sal a Min Sal", DiffMinMax = (Emps.Max(e => e.Sal) - Emps.Min(m => m.Sal)) };


            DataGrid.ItemsSource = result.Take(1).ToList();
        }
        private void Group12(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         group e by e.Deptno into grp
                         where grp.Select(m => m.Ename).Count() > 3
                         select new { Dept = grp.Key };

            DataGrid.ItemsSource = result.ToList();
        }
        private void Group13(object sender, RoutedEventArgs ex)
        {
            var result = (from e in Emps
                         select e.Sal).Count();
            var result2 = ((from e in Emps
                            select e.Sal).Distinct()).Count();
            
            if (result == result2)
            {
                DataGrid.ItemsSource = (from x in Emps select new { IsAllUnique = "True" }).Take(1).ToList();
            }
            else
            {

                DataGrid.ItemsSource = (from x in Emps select new { IsAllUnique = "False" }).Take(1).ToList();
            }

        }
        private void Group14(object sender, RoutedEventArgs ex)
        {
            var result = from x in Emps
                         select new
                         {
                             Str = "Count of Emps in Dallas",
                             Count = (from e in Emps
                              join d in Depts on e.Deptno equals d.Deptno
                              where d.Loc == "DALLAS"
                              select e).Count()
                         };

            DataGrid.ItemsSource = result.Take(1).ToList();
        }
        private void Group15(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         group e by e.Sal into grp
                         where grp.Max(c => c.Sal) < 1000
                         select new { MinSal = grp.Select(e => e.Sal).Min()};

            DataGrid.ItemsSource = result.ToList();
        }

        private void SubQuery1(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         where e.Sal == (Emps.Select(m => m.Sal).Min())
                         select e;

            DataGrid.ItemsSource = result.ToList();
        }

        private void SubQuery2(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         from d in (from m in Emps where m.Ename == "BLACK" select new { m.Job, m.Empno })
                         where e.Job == d.Job
                         select e;

            DataGrid.ItemsSource = result.ToList();
        }

        private void SubQuery3(object sender, RoutedEventArgs ex)
        {

            var result = from e in Emps
                         join d in from m in Emps
                                group m by m.Deptno into grp
                                select new { grp.Key, SalMin = (int)grp.Min(c => c.Sal) } on e.Sal equals d.SalMin
                         where e.Deptno == d.Key
                         select e;

            DataGrid.ItemsSource = result.ToList();
        }

        private void SubQuery4(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         from d in (from m in Emps
                                   group m by m.Deptno into grp
                                   select new { SalMin = grp.Min(c => c.Sal), deptn = grp.Key})
                         where e.Sal == d.SalMin && e.Deptno == d.deptn
                         select e;

            DataGrid.ItemsSource = result.ToList();
        }

        private void SubQuery5(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         where Emps.Any(m => m.Sal > Emps.Where(n => n.Deptno == 30).Min(x => x.Sal))
                         select e;
            DataGrid.ItemsSource = result.ToList();
        }

        private void SubQuery6(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         from d in Emps.Where(n => n.Deptno == 30).Select(x => x.Sal)
                         where e.Sal > d
                         select e;

            DataGrid.ItemsSource = result.ToList();
        }

        private void SubQuery7(object sender, RoutedEventArgs ex)
        {
            var result = from e in (from e1 in Emps
                                    group e1 by e1.Deptno into grp
                                    select new { grp.Key, avg = grp.Average(m => m.Sal) })
                         from d in Depts
                         where e.Key != 30 && d.Deptno == e.Key
                         select new { d.Dname, e.avg };

            DataGrid.ItemsSource = result.ToList();
        }

        private void SubQuery8(object sender, RoutedEventArgs ex)
        {
            var result = from e in (from m in Emps
                                   group m by m.Job into grp
                                   select new { grp.Key, sal = grp.Average(em => em.Sal)})
                         let max = (from m in Emps
                                    group m by m.Job into grp
                                    select new { max = grp.Average(em => em.Sal) }).Max(d => d.max)
                         where e.sal == max
                         select e;

            DataGrid.ItemsSource = result.ToList();
        }

        private void SubQuery9(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         where e.Sal > (from d in Emps
                                        from m in Depts
                                        where d.Deptno == m.Deptno && m.Dname == "SALES"
                                        select d).Max(es => es.Sal)
                         select e;
            DataGrid.ItemsSource = result.ToList();
        }

        private void SubQuery10(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         from d in (from m in Emps
                                    group m by m.Deptno into grp
                                    select new { grp.Key, avg = grp.Average(m => m.Sal) })
                         where e.Deptno == d.Key && e.Sal > d.avg
                         select e;
            DataGrid.ItemsSource = result.ToList();
        }

        private void SubQuery11(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         where Emps.Exists(m => m.Mgr != null)
                         select e;

            DataGrid.ItemsSource = result.ToList();

        }

        private void SubQuery12(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         join d in Depts on e.Deptno equals d.Deptno
                         where e.Deptno != d.Deptno
                         select e;

            DataGrid.ItemsSource = result.ToList();
        }

        private void SubQuery13(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         from d in (from m in Emps
                                    group m by m.Deptno into grp
                                    select new { grp.Key, hire = grp.Select(ms => ms.HireDate).Max() })
                         from x in Depts
                         where e.HireDate == d.hire && d.Key == x.Deptno
                         orderby e.HireDate
                         select new { x.Dname,
                             e.Empno,
                             e.Ename,
                             e.Job,
                             e.Mgr,
                             e.Sal,
                             e.Comm,
                             e.HireDate,
                             e.Deptno,
                         };

            DataGrid.ItemsSource = result.ToList();
        }

        private void SubQuery14(object sender, RoutedEventArgs ex)
        {
            var result = from e in Emps
                         from d in (from m in Emps
                                    group m by m.Deptno into grp
                                    select new { grp.Key, avg = grp.Average(m => m.Sal) })
                         where e.Deptno == d.Key && e.Sal > d.avg
                         select new { e.Ename, e.Sal, e.Deptno };
            DataGrid.ItemsSource = result.ToList();
        }
        
    }
}
