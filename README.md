**Ćwiczenia 6**

W niniejszych ćwiczeniach zajmiemy się wykorzystaniem języka LINQ (ang. Language
Integrated Query) w języku C# w celu odpytywania kolekcji obiektów typu IEnumerable<T> o
wymagane przez nas dane.
W trakcie kolejnych zajęć wykorzystamy język LINQ w celu komunikacji z relacyjną bazą
danych.
Co powinieneś wiedzieć po ćwiczeniach?
* Jak wykonywać proste zapytania z pomocą języka LINQ?
    * Jak korzystać ze składni z użyciem słów kluczowych?
    * Jak korzystać ze składnie wykorzystującej wyrażenia Lambda i Metod rozszerzeń?
* Jak wykonywać zapytania wymagające złączeń?
* Jak wykonywać zapytania filtrujące dane?
* Jak sortować dane?
* Jak wykorzystywać operatory grupujące dane?
* Jak korzystać ze składni wykorzystującej podzapytania?
* Jak korzystać z typów anonimowych?
* Jak korzystać z metod rozszerzeń?
* Jak korzystać z wyrażeń Lambda?

**Zadanie**

Przykładowy projekt WPF zawiera dwie kolekcje obiektów Emp i Dept (być może kojarzycie je z
zajęć RBD).
Waszym zadaniem jest dopisanie kilku metod w kodzie C#, które z pomocą języka LINQ będą
realizować funkcje opisane w poniższych zadaniach.
Przykłady:
https://linqsamples.com/

Za wykonanie wszystkich zapytań – 3 pkt
Wykonujemy min (1 pkt):

* 10- zapytania proste
* 5 – łączenie danych
* 5 – grupowanie
* 5 - podzapytań

**Zapytania proste**

1. Wybrane wyrażenie SAL*12 zaetykietuj nagłówkiem R PENSJA.
2. Połącz EMPNO i nazwisko, opatrz je nagłówkiem EMPLOYEE.
3. Utwórz zapytanie zwracające wynik w postaci np. „Kowalski pracuje w dziale 20”.
4. Wylicz roczną pensję całkowitą dla każdego pracownika (z uwzględnieniem prowizji).
5. Wyświetl wszystkie numery departamentów występujące w tabeli EMP.
6. Wyświetl wszystkie różne numery departamentów występujące w tabeli EMP.
7. Wybierz wszystkie wzajemnie różne kombinacje wartości DEPTNO i JOB.
8. Posortuj wszystkie dane tabeli EMP według ENAME.
9. Posortuj malejąco wszystkie dane tabeli EMP według daty ich zatrudnienia począwszy od ostatnio zatrudnionych.
10. Posortuj dane tabeli EMP według wzrastających wartości kolumn DEPTNO oraz malejących wartości kolumny SAL (bez wypisywania kolumny SAL).
11. Wybierz nazwiska, numery, stanowiska pracy i numery departamentów wszystkich pracowników zatrudnionych na stanowisku CLERK.
12. Wybierz wszystkie nazwy i numery departamentów większe od nr 20.
13. Wybierz pracowników, których prowizja przekracza miesięczną pensję.
14. Wybierz dane tych pracowników, których zarobki mieszczą się pomiędzy 1000 a 2000.
15. Wybierz dane pracowników, których bezpośrednimi szefami są 7902,7566 lub 7788.
16. Wybierz dane tych pracowników, których nazwiska zaczynają się na S.
17. Wybierz dane tych pracowników, których nazwiska są czteroliterowe.
18. Wybierz dane tych pracowników, którzy nie posiadają szefa.
19. Wybierz dane tych pracowników, których zarobki są poza przedziałem <1000,2000>.
20. Wybierz dane tych pracowników, których nazwiska nie zaczynają się na M.
21. Wybierz dane tych pracowników, którzy mają szefa.
22. Wybierz dane tych pracowników zatrudnionych na stanowisku CLERK których zarobki SAL mieszczą się w przedziale <1000.2000).
23. Wybierz dane pracowników zatrudnionych na stanowisku CLERK albo takich, których zarobki SAL mieszczą się w przedziale <1000.2000).
24. Wybierz wszystkich pracowników zatrudnionych na stanowisku MANAGER z pensją
powyżej 1500 oraz wszystkich pracowników na stanowisku SALESMAN, niezależnie od pensji.
25. Wybierz wszystkich pracowników zatrudnionych na stanowisku MANAGER lub na stanowisku SALESMAN lecz zarabiających powyżej 1500.
26. Wybierz wszystkich pracowników zatrudnionych na stanowisku MANAGER ze wszystkich departamentów wraz ze wszystkimi pracownikami zatrudnionymi na stanowisku CLERK w departamencie 10.

**Łączenie danych**

1. Połącz dane z tabel EMP i DEPT przy pomocy INNER JOIN.
2. Wybierz nazwiska oraz nazwy departamentów wszystkich pracowników w kolejności alfabetycznej.
3. Wybierz nazwiska wszystkich pracowników wraz z numerami i nazwami departamentów w których są zatrudnieni.
4. Dla pracowników o miesięcznej pensji powyżej 1500 podaj ich nazwiska, miejsca usytuowania ich departamentów oraz nazwy tych departamentów.
5. Wybierz pracowników zatrudnionych w Dallas.
6. Wypisz dane wszystkich działów oraz ich pracowników tak, aby dane działu pojawiły się, nawet jeśli nie ma w dziale żadnego pracownika.
7. Wypisz dane wszystkich działów oraz ich pracowników tak, aby dane pracownika pojawiły się, nawet jeśli pracownik nie jest przypisany do działu.
8. Wybierz pracowników (nazwisko, numer działu) z działu 30 i 20.Wypisz dział 20 bez nazwisk.
9. Wypisz stanowiska występujące w dziale 10 oraz 30.
10. Wypisz stanowiska występujące zarówno w dziale 10 jak i 30.
11. Wypisz stanowiska występujące w dziale 10 a nie występujące w dziale 30.
12. Wybierz pracowników, którzy zarabiają mniej od swoich kierowników.
13. Dla każdego pracownika wypisz jego nazwisko oraz nazwisko jego szefa. Posortuj według nazwiska szefa.

**Grupowanie**

1. Oblicz średni zarobek w firmie.
2. Znajdź minimalne zarobki na stanowisku CLERK.
3. Znajdź ilu pracowników zatrudniono w departamencie 20.
4. Oblicz średnie zarobki na każdym ze stanowisk pracy.
5. Oblicz średnie zarobki na każdym ze stanowisk pracy z wyjątkiem stanowiska MANAGER.
6. Oblicz średnie zarobki na każdym ze stanowisk pracy w każdym departamencie.
7. Dla każdego stanowiska oblicz maksymalne zarobki.
8. Wybierz średnie zarobki tylko tych departamentów, które zatrudniają więcej niż trzech pracowników.
9. Wybierz tylko te stanowiska, na których średni zarobek wynosi 3000 lub więcej.
10. Znajdź średnie miesięczne pensje oraz średnie roczne zarobki dla każdego stanowiska, pamiętaj o prowizji.
11. Znajdź różnicę miedzy najwyższą i najniższa pensją.
12. Znajdź departamenty zatrudniające powyżej trzech pracowników.
13. Sprawdź, czy wszystkie numery pracowników są rzeczywiście wzajemnie różne.
14. Podaj najniższe pensje wypłacane podwładnym swoich kierowników. Wyeliminuj grupy o minimalnych zarobkach niższych niż 1000. Uporządkuj według pensji.
15. Wypisz ilu pracowników ma dział mający siedzibę w DALLAS.

**Podzapytania**

1. Znajdź pracowników z pensją równą minimalnemu zarobkowi w firmie.
2. Znajdź wszystkich pracowników zatrudnionych na tym samym stanowisku co BLAKE.
3. Znajdź pracowników o pensjach z listy najniższych zarobków osiągalnych w departamentach.
4. Znajdź pracowników o najniższych zarobkach w ich departamentach.
5. Stosując operator ANY wybierz pracowników zarabiających powyżej najniższego zarobku z departamentu 30.
6. Znajdź pracowników, których zarobki są wyższe od pensji każdego pracownika z departamentu 30.
7. Wybierz departamenty, których średnie zarobki przekraczają średni zarobek departamentu 30.
8. Znajdź stanowisko, na którym są najwyższe średnie zarobki.
9. Znajdź pracowników, których zarobki przekraczają najwyższe pensje z departamentu SALES.
10. Znajdź pracowników, którzy zarabiają powyżej średniej w ich departamentach.
11. Znajdź pracowników, którzy posiadają podwładnych za pomocą operatora EXISTS.
12. Znajdź pracowników, których departament nie występuje w tabeli DEPT.
13. Wskaż dla każdego departamentu ostatnio zatrudnionych pracowników. Uporządkuj według dat zatrudnienia.
14. Podaj ename, sal i deptno dla pracowników, których zarobki przekraczają średnią ich departamentów.